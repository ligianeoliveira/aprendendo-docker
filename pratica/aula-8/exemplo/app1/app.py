import mysql.connector
import json
import os
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
  return 'Hello, Docker! Eu sou o app1'

@app.route('/alunos')
def get_alunos():
  mydb = mysql.connector.connect(
    host="mysqldb",
    user=os.getenv("DATABASE_USER"),
    password=os.getenv("DATABASE_PASSWORD"),
    database=os.getenv("DATABASE_NAME")
  )
  cursor = mydb.cursor()

  cursor.execute("SELECT * FROM aluno")

  row_headers=[x[0] for x in cursor.description] 

  results = cursor.fetchall()
  json_data=[]
  for result in results:
    json_data.append(dict(zip(row_headers,result)))

  cursor.close()

  return json.dumps(json_data)


@app.route('/initdb')
def db_init():
  mydb = mysql.connector.connect(
    host="mysqldb",
    user=os.getenv("DATABASE_USER"),
    password=os.getenv("DATABASE_PASSWORD")
  )
  cursor = mydb.cursor()

  cursor.execute("DROP DATABASE IF EXISTS aula")
  cursor.execute("CREATE DATABASE aula")
  cursor.close()

  mydb = mysql.connector.connect(
    host="mysqldb",
    user=os.getenv("DATABASE_USER"),
    password=os.getenv("DATABASE_PASSWORD"),
    database=os.getenv("DATABASE_NAME")
  )
  cursor = mydb.cursor()

  cursor.execute("DROP TABLE IF EXISTS aluno")
  cursor.execute("CREATE TABLE aluno (nome VARCHAR(255), curso VARCHAR(255))")

  cursor.execute("INSERT INTO aluno (nome, curso) VALUES ('Aluno 1', 'Python')")
  cursor.execute("INSERT INTO aluno (nome, curso) VALUES ('Aluno 2', 'Docker')")

  mydb.commit()

  cursor.close()

  return 'banco iniciado no app1'

if __name__ == "__main__":
  app.run(host ='0.0.0.0')