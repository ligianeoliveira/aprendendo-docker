import mysql.connector
import json
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
  return 'Hello, Docker!'

@app.route('/initdb')
def db_init():
  mydb = mysql.connector.connect(
    host="mysqldb",
    user="root",
    password="s3nh@"
  )
  cursor = mydb.cursor()

  cursor.execute("DROP DATABASE IF EXISTS aula")
  cursor.execute("CREATE DATABASE aula")
  cursor.close()

  mydb = mysql.connector.connect(
    host="mysqldb",
    user="root",
    password="s3nh@",
    database="aula"
  )
  cursor = mydb.cursor()

  cursor.execute("DROP TABLE IF EXISTS aluno")
  cursor.execute("CREATE TABLE aluno (nome VARCHAR(255), curso VARCHAR(255))")

  cursor.execute("INSERT INTO aluno (nome, curso) VALUES ('Aluno 1', 'Python')")
  cursor.execute("INSERT INTO aluno (nome, curso) VALUES ('Aluno 2', 'Docker')")

  mydb.commit()

  cursor.close()

  return 'banco iniciado'

@app.route('/alunos')
def get_alunos():
  mydb = mysql.connector.connect(
    host="mysqldb",
    user="root",
    password="s3nh@",
    database="aula"
  )
  cursor = mydb.cursor()

  cursor.execute("SELECT * FROM aluno")
 
  #pega as colunas da tabela do banco, como nome e curso
  row_headers=[x[0] for x in cursor.description] 

  #pega todos o resultado que veio do select e salva na variável results
  results = cursor.fetchall()
  #cria uma variável json_data
  json_data=[]

  #percorrendo o results, e a cada linha ele vai salvar no json_data
  for result in results:
    #append: adiciona itens na variável
    #zip: junta as informações do cabeçalho e dados
    #dic: o zip é convertido em dicionário
    json_data.append(dict(zip(row_headers,result)))

  cursor.close()
  
  #dumps é o método que vai pega o dicionário e converter em json e retornar o resultado
  return json.dumps(json_data)


if __name__ == "__main__":
  app.run(host ='0.0.0.0')